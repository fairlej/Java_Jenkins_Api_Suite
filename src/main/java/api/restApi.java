package api;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import static org.fest.assertions.Assertions.assertThat;

public class restApi {

    // HTTP GET request
    public String getPetById(int id) throws Exception {

        String url = "https://petstore.swagger.io/v2/pet/" + id;

        URL obj = new URL(url);
        HttpURLConnection httpsCon = (HttpURLConnection) obj.openConnection();
        httpsCon.setRequestMethod("GET");

        int responseCode = httpsCon.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);
        // verify 200 response code
        assertThat(httpsCon.getResponseCode()).isEqualTo(200);

        BufferedReader br = new BufferedReader(new InputStreamReader((httpsCon.getInputStream())));
        StringBuilder sb = new StringBuilder();
        String output;
        while ((output = br.readLine()) != null) {
            sb.append(output);
        }
        br.close();
        return sb.toString();
    }

    // HTTP POST request
    public String createPet(int randomId, String name, String status) throws Exception {

        String url = "https://petstore.swagger.io/v2/pet";
        URL obj = new URL(url);
        HttpsURLConnection httpsCon = (HttpsURLConnection) obj.openConnection();

        //add headers
        httpsCon.setRequestMethod("POST");
        httpsCon.setRequestProperty("Content-Type", "application/json");

        String postBody = "{\"id\":" + randomId + ",\"name\": \"" + name + "\",\"status\": \"" + status + "\"}";

        // Send post request
        httpsCon.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(httpsCon.getOutputStream());
        wr.writeBytes(postBody);
        wr.flush();
        wr.close();

        int responseCode = httpsCon.getResponseCode();
        System.out.println("\nSending 'POST' request to : " + url);
        System.out.println("Post body : " + postBody);
        System.out.println("Response Code : " + responseCode);
        //verify 200 response code
        assertThat(httpsCon.getResponseCode()).isEqualTo(200);

        BufferedReader br = new BufferedReader(new InputStreamReader((httpsCon.getInputStream())));
        StringBuilder sb = new StringBuilder();
        String output;
        while ((output = br.readLine()) != null) {
            sb.append(output);
        }
        br.close();
        return sb.toString();
    }

}