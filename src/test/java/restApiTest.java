
import api.restApi;
import org.testng.annotations.Test;


import java.util.Random;

import static org.fest.assertions.Assertions.assertThat;


@Test(groups = {"smoke_test"})
public class restApiTest {

    public static void main(String[] args) throws Exception {

        restApi http = new restApi();
        Random rand = new Random();
        int randomId = rand.nextInt(1000) + 1;

        // create a pet and verify response code and params passed in
        String postResponse = http.createPet(randomId,"Charlie The Dog", "available");
        assertThat(postResponse.contains("Charlie The Dog")).isTrue();
        assertThat(postResponse.contains("available")).isTrue();
        System.out.println("Response body: " + postResponse);

        // get the pet and verify name and status from the creation of it above.
        String getResponse =  http.getPetById(randomId);
        assertThat(getResponse.contains("Charlie The Dog")).isTrue();
        assertThat(getResponse.contains("available")).isTrue();
        System.out.println("Response body: " + getResponse);

    }

}